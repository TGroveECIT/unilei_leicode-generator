﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unilei_leicode_generator.Services;
using unilei_leicode_GLEIFAPI_helper.Level_1_Information;

namespace unilei_leicode_console_helper
{
    class Program
    {
        static void Main(string[] args)
        {
            string code = LEICalculator.CreateLEICode();
            Console.WriteLine(code);
            bool found;
            string someExistingLeiCode = "529900W18LQJJN6SJ336";
            found = Level1InformationAPI.LEICodeExists(someExistingLeiCode);

            if (found) Console.WriteLine(someExistingLeiCode + " Exists in dataset");
            else Console.WriteLine(someExistingLeiCode + " does not exist in dataset");
            
                
            found = Level1InformationAPI.LEICodeExists(code);
            if (found) Console.WriteLine(code + " Exists in dataset");
            else Console.WriteLine(code + " does not exist in dataset"); 

        }
    }
}
