﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace unilei_leicode_GLEIFAPI_helper.Level_1_Information
{
    public static class Level1InformationAPI
    {
        private static string url = GLEIFAPIHelper.BASEURL + "lei-records/";
        public static bool LEICodeExists(string code)
        {
            var client = new RestClient($"{url}{code}");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/vnd.api+json");
            IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return false;
            }
            return true;
        }
    }
}
