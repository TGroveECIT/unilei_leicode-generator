﻿using System;

namespace unilei_leicode_generator
{
    public static class StaticRandom
    {
        private static DateTime now = DateTime.Now;
        public static Random Random = new Random(int.Parse($"{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Millisecond}"));
    }
}
