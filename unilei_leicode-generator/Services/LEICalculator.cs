﻿using MathNet.Numerics;
using System;
using System.Numerics;
using System.Text;
using unilei_leicode_generator.Dictionaries;

namespace unilei_leicode_generator.Services
{


    public static class LEICalculator
    {
        /// <summary>
        /// Move identifier to a safe place when implementing for real
        /// </summary>
        private static string identifier = "1234";
        private static string LOUIdentifier { get { return identifier; } }
        private static BigInteger EUCLIDIANDIVISOR = 97;
        private static BigInteger REMAINDERSUBSTRACTION = EUCLIDIANDIVISOR + 1;

        //CreationDate skal tilføjes efter godkendt oprettelse????
        //Fra dokumentationen: The entity creation date, being the date on which the entity was first established, as represented in the ISO 8601 series. Where an LEI is required to be assigned before the entity is legally formed, this information should be added as soon as possible after its creation.


        /// <summary>
        /// Creates a new LEI Code, it is not checked if it is actually available
        /// </summary>
        /// <returns></returns>
        public static string CreateLEICode()
        {
            //create base LEI without Identifier
            string baseLei = CalcLEI();

            //Create digit pair string for later calculation of identifier.
            string digitPairString = ConvertLEICodeToDigitPairString(baseLei);

            //Calculate the remainder for identifier, 00 is appended to calculate the identifier
            BigInteger digitPairBigInt = BigInteger.Parse(digitPairString + "00");
            BigInteger remainder = REMAINDERSUBSTRACTION - EuclidRemainder(digitPairBigInt);

            digitPairBigInt += remainder;
            BigInteger newRemainder = EuclidRemainder(digitPairBigInt);


            if (newRemainder != 1)
            {
                //what then, remainder has not been calculated properly...
                //This should not happen
                //if so return default.. or throw error maybe?
                Console.WriteLine(baseLei + newRemainder.ToString());
                return default;
            }
            else
            {
                return baseLei + remainder.ToString();
            }
        }

        //DELETE THIS METHOD MAYBE
        /// <summary>
        /// 
        /// </summary>
        /// <param name="legalEntityName">The name of the legal entity as recorded in an official register, or otherwise in the entity’s constituting documents.</param>
        /// <param name="entityLegalFormCode">The legal form of the entity as represented in ISO 20275. 
        /// https://www.gleif.org/en/about-lei/code-lists/iso-20275-entity-legal-forms-code-list
        /// </param>
        /// <param name="countryCode">The address of legal formation and the jurisdiction in which it is located as represented in the ISO 3166 series. 
        /// <seealso cref="https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes"/></param>
        /// <param name="officialRegister">Where applicable, the name of the official register in which the legal entity is registered and its identifier in that registry.</param>
        /// <param name="headquarterAddress">The address of the headquarters of the legal entity or the international branch.</param>
        /// <param name="parentIdentity">Identification of the direct and ultimate parent or reason why these are not identified in the public record. In some circumstances, information regarding parent entities may be collected for regulatory use but not included in the public LEI data record. There are also reasons why parent information may not be collected, including where there is no parent entity or the child entity is exempt from submitting the data for exceptional reasons.</param>
        /// <param name="internationalBranchLEI">In the case of an international branch, the LEI of the entity of which it is a branch.</param>
        /// <param name="investmentFund">In the case of an investment fund, the LEI of the fund management entity.</param>
        /// <param name="investmentSubFund">In the case of an investment fund that is also a sub-fund of an umbrella structure, the LEI of the umbrella structure.</param>
        /// <param name="investmentfeederFund">In the case of an investment fund that is also a feeder fund in a master-feeder relationship, the LEI of the master fund, when available.</param>
        /// <param name="legalEntity">The status of the legal entity regarding whether or not the entity is legally registered, or otherwise constituted, and operating.</param>
        /// <param name="validationStatus">The status of the validation, publication and updating of the LEI and its supporting data record.</param>
        /// <param name="firstLEIAssignment">The date of the first LEI assignment, being the date of publication of the identifier and its supporting data record as represented in the ISO 8601 series.</param>
        /// <param name="lastLEIUpdate">The date of last update of the LEI data record as represented in the ISO 8601 series, and the reason for the update.</param>
        /// <param name="effectiveDate">The effective date of a legal entity event, being the date on which the event was effective in the legal jurisdiction in which the entity was formed, as represented in the ISO 8601 series.</param>
        /// <returns>Returns a string consisting of a new LEI Code</returns>
        private static string CalcLEI(string legalEntityName, string entityLegalFormCode, string countryCode, string headquarterAddress, string parentIdentity, string legalEntity, string validationStatus, DateTime effectiveDate, string internationalBranchLEI = "", string investmentFund = "", string investmentSubFund = "", string investmentfeederFund = "", string officialRegister = "", DateTime firstLEIAssignment = default, DateTime lastLEIUpdate = default)
        {
            return null;
        }


        private static string CalcLEI ()
        {
            

            StringBuilder res = new StringBuilder();
            res.Append(LOUIdentifier);
            for (int i = 0; i < 14; i++)
            {
                res.Append(LEIDigitPairsDictionary.IntToCharDict[StaticRandom.Random.Next(0, 35)]);
            }

            return res.ToString();
        }
        
        /// <summary>
        /// WORST NAME EVER...
        /// </summary>
        /// <param name="strToConvert"></param>
        /// <returns></returns>
        private static string ConvertLEICodeToDigitPairString(string strToConvert)
        {
            strToConvert = strToConvert.ToUpper();
            string convertetString = "";
            for (int i = 0; i < strToConvert.Length; i++)
            {
                convertetString += LEIDigitPairsDictionary.CharToIntDict[strToConvert[i]].ToString();
            }
            return convertetString;
        }


        /// <summary>
        /// returns the euclidean remainder where the divisor is preset to the value of <see cref="EUCLIDIANDIVISOR"/>
        /// </summary>
        /// <param name="a">The dividend</param>
        /// <returns></returns>
        private static BigInteger EuclidRemainder(BigInteger a)
        {
            return Euclid.Remainder(a, EUCLIDIANDIVISOR);
            //return a % EUCLIDIANDIVISOR;
        }
        /// <summary>
        /// returns the euclidean modulus where the divisor is preset to the value of <see cref="EUCLIDIANDIVISOR"/>
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        private static BigInteger EuclidModulus(BigInteger a)
        {
            return Euclid.Modulus(a, EUCLIDIANDIVISOR);
            //return a % EUCLIDIANDIVISOR;
        }


    }
}
